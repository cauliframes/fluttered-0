import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  final title = 'An App';

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.black),
    );

    return MaterialApp(
      title: title,
      home: Mentors(),
      theme: _buildTheme(),
    );
  }

  ThemeData _buildTheme() {
    final baseTheme = ThemeData.light();
    final textTheme = baseTheme.textTheme.apply(
      bodyColor: Colors.black,
      displayColor: Colors.black,
      decorationColor: Colors.black,
    );
    final floatingActionButtonTheme = baseTheme.floatingActionButtonTheme
        .copyWith(backgroundColor: Colors.white);
    final iconTheme = baseTheme.iconTheme.copyWith(color: Colors.black);
    return baseTheme.copyWith(
      backgroundColor: Colors.white,
      primaryColor: Colors.white,
      accentColor: Colors.white,
      floatingActionButtonTheme: floatingActionButtonTheme,
      iconTheme: iconTheme,
      primaryIconTheme: iconTheme,
      textTheme: textTheme,
      primaryTextTheme: textTheme,
      accentTextTheme: textTheme,
    );
  }
}

class Mentors extends StatefulWidget {
  const Mentors({Key key}) : super(key: key);

  final String title = 'mentors';
  final List<Mentor> mentors = const [
    Mentor('Imam', 24, 70000),
    Mentor('Sani', 20, 70000),
    Mentor('Faishal', 33, 90000),
    Mentor('Mamed', 25, 110000)
  ];

  @override
  _MentorsState createState() => _MentorsState();
}

class _MentorsState extends State<Mentors> {
  Mentor selected;

  String get title => widget.title;

  List<Mentor> get mentors => widget.mentors;

  bool get mentorSelected => selected != null;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final scaffold = Scaffold(
      backgroundColor: theme.primaryColor,
      appBar: AppBar(
          elevation: 0,
          leading: RotatingSnowflake(),
          title: Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w300,
              letterSpacing: 1,
              fontSize: 24,
              decoration: TextDecoration.combine(
                [TextDecoration.underline, TextDecoration.overline],
              ),
            ),
          ),
          centerTitle: true),
      body: Stack(
        children: [
          Center(
            child: OverflowBox(
              maxWidth: double.infinity,
              child: AnimatedPadding(
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeInOutCirc,
                padding: EdgeInsets.only(
                  bottom: 88,
                  right: !mentorSelected ? 512 : 0,
                ),
                child: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.shopping_cart,
                    color: Colors.black,
                    size: 88,
                  ),
                ),
              ),
            ),
          ),
          AnimatedPadding(
            duration: const Duration(milliseconds: 400),
            curve: Curves.ease,
            padding: EdgeInsets.only(
              left: 32,
              top: !mentorSelected ? 32 : 450,
              right: 32,
            ),
            child: Material(
              elevation: 4,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                topLeft: Radius.circular(32),
                topRight: Radius.circular(32),
              )),
              child: !mentorSelected
                  ? ListView.separated(
                      separatorBuilder: (_, __) => Divider(
                        color: Colors.black,
                      ),
                      padding: EdgeInsets.all(44),
                      itemCount: mentors.length,
                      itemBuilder: (context, index) {
                        final mentor = mentors[index];
                        return ListTile(
                          leading: Container(
                            padding: EdgeInsets.all(1.5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.black),
                            child: selected != mentor
                                ? CircleAvatar(
                                    backgroundColor: theme.primaryColor,
                                    child: Text(
                                      mentor.nameInitial,
                                      style: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  )
                                : CircleAvatar(
                                    backgroundColor: Colors.black,
                                    child: Icon(
                                      Icons.done,
                                      color: theme.primaryColor,
                                    ),
                                  ),
                          ),
                          title: Text(
                            mentor.nameWithGender,
                            style: TextStyle(letterSpacing: 1),
                          ),
                          subtitle: Text(
                            mentor.priceWithCurrency,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              letterSpacing: 2,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              selected = selected == mentor ? null : mentor;
                            });
                          },
                        );
                      },
                    )
                  : ListView(
                      padding: EdgeInsets.all(44),
                      children: [
                        ListTile(
                          leading: Container(
                            padding: EdgeInsets.all(1.5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.black),
                            child: CircleAvatar(
                              backgroundColor: Colors.black,
                              child: Icon(
                                Icons.done,
                                color: theme.primaryColor,
                              ),
                            ),
                          ),
                          title: Text(
                            selected.nameWithGender,
                            style: TextStyle(letterSpacing: 1),
                          ),
                          subtitle: Text(
                            selected.priceWithCurrency,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              letterSpacing: 2,
                            ),
                          ),
                          onTap: () {
                            setState(() => selected = null);
                          },
                        )
                      ],
                    ),
            ),
          )
        ],
      ),
    );
    return scaffold;
  }
}

class RotatingSnowflake extends StatefulWidget {
  @override
  _RotatingSnowflakeState createState() => _RotatingSnowflakeState();
}

class _RotatingSnowflakeState extends State<RotatingSnowflake>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1, milliseconds: 500),
      vsync: this,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: CurvedAnimation(
        parent: _controller..forward(),
        curve: Curves.fastLinearToSlowEaseIn,
      ),
      child: Icon(Icons.ac_unit),
    );
  }
}

class Mentor {
  const Mentor(this.name, this.age, this.price);

  final String name;
  final int age;
  final int price;

  String get nameInitial => name[0].toUpperCase();

  String get nameWithGender => '👨 $name';

  String get priceWithCurrency => '💸 Rp $price';
}
